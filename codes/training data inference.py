

import sys
sys.path.append('../detectron2')
sys.path.append('../../parseltongue-ner')

from parseltongue.utils.helpers import Timer
from parseltongue.parsing.parsed_doc import ParsedDoc
#from parseltongue.bvc.bvc import BitVectorComputation
from time import time
import os
import numpy as np
from datetime import datetime
import pandas as pd
import detectron2
import torch, torchvision
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog
from pdf2image import convert_from_path

from time import time
import os

MAX_PAGES_TO_PARSE = 10

class read_resume_image_predict:
    def __init__(self, source_pth, device='cpu', source_type='folder', file_no_limit=10000):
        # Source can be 'folder' or manual 'json'
        self.source = source_pth
        self.source_type = source_type
        self.file_no_limit = file_no_limit
        #self.bvc = BitVectorComputation()

        self.cfg = get_cfg()
        # add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library

        self.cfg.merge_from_file('../DLA_mask_rcnn_R_50_FPN_3x.yaml')
        # cfg.merge_from_file('./configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml')

        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
        # Find a model from detectron2's model zoo. You can use the https://dl.fbaipublicfiles... url as well
        self.cfg.MODEL.WEIGHTS = '../model_final_trimmed.pth'
        # cfg.MODEL.WEIGHTS = 'model_final_f10217.pkl'
        self.cfg.MODEL.DEVICE = device
        self.predictor = DefaultPredictor(self.cfg)

        self.classes = ['text', 'title', 'list', 'table', 'figure']
        self.extract_pths_from_source()


    def extract_pths_from_source(self):
        self.resume_image_tables = {}
        self.resume_image_tables['file'] = []
        self.resume_image_tables['page_no'] = []
        self.resume_image_tables['image_dimension'] = []
        self.resume_image_tables['features'] = []
        self.resume_image_tables['tables_pdfminer'] = []
        #self.resume_image_tables['image'] = []
        for cls in self.classes:
            self.resume_image_tables[cls] = []
        dt = datetime.now()
        if self.source_type == 'folder':
            fils = os.listdir(self.source)
            ind, ind_txt = 0, 0
            print(len(fils))
            while ind_txt < self.file_no_limit and ind < len(fils):
                self.pth = self.source + '/' + fils[ind]



                images, features, bbox_pdfminer = self.extract_image()
                if images:
                    # Predict
                    ind_txt += 1
                    for pg, image in enumerate(images):
                        pred_inst = self.predictor(image)
                        self.resume_image_tables['file'].append(fils[ind])
                        self.resume_image_tables['page_no'].append(pg)
                        self.resume_image_tables['image_dimension'].append(image.shape)
                        self.resume_image_tables['features'].append(features)
                        self.resume_image_tables['tables_pdfminer'].append(bbox_pdfminer[pg])
                        for ind_cls, cls in enumerate(self.classes):
                            self.resume_image_tables[cls].append([j for i,j
                                                             in zip(pred_inst['instances'].
                                                                     pred_classes.tolist(),
                                                                    pred_inst['instances'].
                                                                     pred_boxes.tensor.tolist())
                                                                  if i == ind_cls])
                    pd.DataFrame(self.resume_image_tables).to_csv('../data/resume_image_'
                                                                   + str(dt) + '.csv')

                ind += 1
        else:
            # Extract from json
            pass



    def convert_store_pdf(self, url, pth='../parseltongue-pkg/pdf_manual/temp/'):
        is_success = 32512
        if type(url) is str:
            convert_command = '/Applications/LibreOffice.app/Contents/MacOS/soffice --headless --convert-to pdf --outdir {} {}'

            # DOWNLOAD FILE
            resume_name = url.split('/')[-1]
            is_success = os.system('wget {} -O ../parseltongue-pkg/pdf_manual/temp/{}'.format(url, resume_name))
            if not resume_name.endswith('.pdf'):
                is_success = os.system(convert_command.format(pth, pth + resume_name))
                resume_name = resume_name.replace('.' + resume_name.split('.')[-1], '.pdf')

    def extract_image(self, file_type='pdf'): # Extract only 'pdf' or all
        convert_command = '/Applications/LibreOffice.app/Contents/MacOS/soffice --headless --convert-to pdf --outdir {} {}'
        txt = None
        # DOWNLOAD FILE if its a link

        if 'http:' in self.pth or 'https:' in self.pth:
            is_success, resume_name = self.convert_store_pdf(url)
            pth = '../parseltongue-pkg/pdf_manual/temp/'
            self.pth = pth + resume_name

        if not self.pth.endswith('.pdf') and file_type == 'all':
            resume_name = self.pth.split('/')[-1]
            pth = '/'.join(self.pth.split('/')[0:-1])

            if len(pth.strip()) == 0:
                pth = '.'

            if '.' in resume_name[-4:]:
                resume_name = resume_name.replace('.' + resume_name.split('.')[-1], '.pdf')
            else:
                resume_name = resume_name + '.pdf'

            self.pth = pth + '/' + resume_name
        image_array = []
        features = []
        bbox_pdfminer = {}
        if self.pth.endswith('.pdf'):
            try:
                #images = convert_from_path(self.pth)
                pd = ParsedDoc(self.pth, MAX_PAGES_TO_PARSE, 20)
                pd.load()
                # ParsedDoc
                features = pd.features
                img_shape = {}
                for page_id, page in enumerate(pd.tx.pages):
                    image_array.append(np.array(page))
                    img_shape[page_id] = image_array[-1].shape

                for pg in pd.table_bboxes:
                    bbox_pdfminer[pg] = []
                    x_ratio = img_shape[pg][1]/pd.layouts[pg].bbox[2]
                    y_ratio = img_shape[pg][0]/pd.layouts[pg].bbox[3]
                    for table_id in pd.table_bboxes[pg]:
                        pd.table_bboxes[pg][table_id] = [pd.table_bboxes[pg][table_id][0]* x_ratio, pd.table_bboxes[pg][table_id][1]
                                                         *y_ratio, pd.table_bboxes[pg][table_id][2] *x_ratio,
                                                         pd.table_bboxes[pg][table_id][3] * y_ratio]
                        bbox_pdfminer[pg].append(pd.table_bboxes[pg][table_id])


            except:
                pass

        return image_array, features, bbox_pdfminer



start = time()


path = '../../../../Data/resumes_dump'

path = '../../parseltongue-pkg/mimir_debug_csv/prod_resumes'

print(os.getcwd())

resume_txt_obj = read_resume_image_predict(path)

end = time()
print(end - start)
